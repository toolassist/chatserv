/*
 * All rights reserved.
 *
 * Copyright (c) 2019 Anthony Anderson
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package net.toolassist.jchatserv.bridge.twitch.chat.tags;

import net.toolassist.jchatserv.bridge.twitch.chat.Tag;
import net.toolassist.jchatserv.bridge.twitch.chat.TaggedMsg;

public class PrivMsgTag extends TaggedMsg
{
    public PrivMsgTag(Tag tag, String msg)
    {
        super(tag, msg);
    }

    public String getBadges()
    {
       return this.getToken("@badges");
    }

    public String getBits()
    {
        return this.getToken("bits");
    }

    public String getColor()
    {
        return this.getToken("color");
    }

    public String getDisplayName()
    {
        return this.getToken("display-name");
    }

    public String getEmotes()
    {
        return this.getToken("emotes");
    }

    public String getId()
    {
        return this.getToken("id");
    }

    public String getMod()
    {
        return this.getToken("mod");
    }

    public String getRoomId()
    {
        return this.getToken("room-id");
    }

    public String getSubscriber()
    {
        return this.getToken("subscriber");
    }

    public String getTwitchTMI()
    {
        return this.getToken("tmi-sent-ts");
    }

    public String getTurbo()
    {
        return this.getToken("turbo");
    }

    public String getUserId()
    {
        return this.getToken("user-id");
    }
}
