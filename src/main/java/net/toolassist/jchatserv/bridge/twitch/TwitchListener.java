/*
 * All rights reserved.
 *
 * Copyright (c) 2019 Anthony Anderson
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package net.toolassist.jchatserv.bridge.twitch;

import net.toolassist.jchatserv.bridge.twitch.chat.TagHandler;
import net.toolassist.jchatserv.config.Config;

import java.net.http.WebSocket;
import java.net.http.WebSocket.Listener;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletionStage;

public class TwitchListener implements Listener
{
    private String pass;
    private String user;
    private List<String> channels;

    public TwitchListener(Config config)
    {
        this.pass = config.twitchConf.token;
        this.user = config.twitchConf.user;
        channels = Arrays.asList(config.twitchConf.defaultChannels);
    }

    @Override
    public void onOpen(WebSocket socket)
    {
        sendRawnl(socket, "CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership");
        sendRawnl(socket, "PASS " + this.pass);
        sendRawnl(socket, "NICK " + user);
        channels.forEach(chan ->sendRawnl(socket, "JOIN #" + chan));
        Listener.super.onOpen(socket);
    }

    @Override
    public CompletionStage<?> onText(WebSocket webSocket, CharSequence data, boolean last)
    {
//        System.out.println(data.toString());
//        new TagHandler(data.toString());
        new TagHandler(data.toString());
        return WebSocket.Listener.super.onText(webSocket, data, last);
    }

    @Override
    public CompletionStage<?> onClose(WebSocket webSocket, int statusCode, String reason)
    {
        return WebSocket.Listener.super.onClose(webSocket, statusCode, reason);
    }

    private void sendRaw(WebSocket socket, String string)
    {
        socket.sendText(string, true);
    }

    private void sendRawnl(WebSocket socket, String string)
    {
        sendRaw(socket,string + "\n");
    }
}
