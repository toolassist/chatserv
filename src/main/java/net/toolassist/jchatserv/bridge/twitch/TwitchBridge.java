/*
 * All rights reserved.
 *
 * Copyright (c) 2019 Anthony Anderson
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package net.toolassist.jchatserv.bridge.twitch;

import net.toolassist.jchatserv.bridge.IBridge;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.WebSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TwitchBridge implements IBridge
{

    private ExecutorService executorService;
    private HttpClient client;
    private WebSocket socket;
    private boolean enabled = false;

    public TwitchBridge()
    {

        executorService = Executors.newFixedThreadPool(4);
        client = HttpClient.newBuilder().executor(executorService).build();
    }

    @Override
    public String getName()
    {
        return "twitch";
    }

    @Override
    public void enable()
    {
        var twitchURI = "wss://irc-ws.chat.twitch.tv";
        if (!this.isEnabled())
        {

            socket = client
                    .newWebSocketBuilder()
                    .buildAsync(URI.create(twitchURI), new TwitchListener(getConfig()))
                    .join();

        }
    }

    @Override
    public void disable()
    {
        if (this.isEnabled())
        {
            socket.sendClose(WebSocket.NORMAL_CLOSURE, "ok").thenRun(()-> System.out.println("disabled"));
        }
    }

    @Override
    public boolean isEnabled()
    {
        return this.enabled;
    }
}
