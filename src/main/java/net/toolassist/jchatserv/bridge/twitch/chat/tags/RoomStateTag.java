/*
 * All rights reserved.
 *
 * Copyright (c) 2019 Anthony Anderson
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package net.toolassist.jchatserv.bridge.twitch.chat.tags;

import net.toolassist.jchatserv.bridge.twitch.chat.Tag;
import net.toolassist.jchatserv.bridge.twitch.chat.TaggedMsg;

public class RoomStateTag extends TaggedMsg
{
    public RoomStateTag(Tag tag, String msg)
    {
        super(tag, msg);
    }

    public String getLang()
    {
        return this.getToken("broadcaster-lang");
    }

    public String emoteOnly()
    {
        return this.getToken("emote-only");
    }

    public String getFollowerOnly()
    {
        return this.getToken("followers-only");
    }

    public String getR9K()
    {
        return this.getToken("r9k");
    }

    public String getSlow()
    {
        return this.getToken("slow");
    }

    public String getSubsOnly()
    {
        return this.getToken("subs-only");
    }
}