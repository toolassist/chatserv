/*
 * All rights reserved.
 *
 * Copyright (c) 2019 Anthony Anderson
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package net.toolassist.jchatserv.bridge.twitch.chat;

import net.toolassist.jchatserv.bridge.twitch.chat.tags.PrivMsgTag;
import net.toolassist.jchatserv.bridge.twitch.chat.tags.RoomStateTag;

import java.util.logging.Logger;

public class TagHandler
{

    private static Logger LOGGER = Logger.getLogger("[TAG LOGGER]");

    private static Tag tag;

    public TagHandler(String msg)
    {
//        System.out.println(msg);
        var split = msg.split("tmi.twitch.tv")[1].split(" ")[1];
//        split[2];

        switch (split) {
            case "ROOMSTATE" : tag = Tag.ROOMSTATE;
                var rst = new RoomStateTag(tag, msg);
                System.out.println(rst.getSlow());
                break;
            case "CLEARCHAT" : tag = Tag.CLEARCHAT;
                break;
            case "GLOBALUSERSTATE": tag = Tag.GLOBALUSERSTATE;
                //NO-OP this we have no need for this right now
                break;
            case "USERNOTICE": tag = Tag.USERNOTICE;
                break;
            case "USERSTATE": tag = Tag.USERSTATE;
                break;
            case "PRIVMSG": tag = Tag.PRIVMSG;
                var priv = new PrivMsgTag(tag, msg);
                var fmt = String.format("Channel %s, %s: " + "%s", priv.getChannel(), priv.getDisplayName(), priv.getChat());
                System.out.println(fmt);
                System.out.println(priv.getEmotes());

                break;
            case "CLEARMSG": tag = Tag.CLEARMSG;
                break;
            default: tag = Tag.UNKNOWN;
                break;
        }

    }

}
