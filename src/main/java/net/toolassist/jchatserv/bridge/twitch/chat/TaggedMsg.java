/*
 * All rights reserved.
 *
 * Copyright (c) 2019 Anthony Anderson
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package net.toolassist.jchatserv.bridge.twitch.chat;

import java.util.Arrays;
import java.util.HashMap;

public class TaggedMsg
{
    private String meta;
    private String chat;
    private String channel;
    private Tag tag;
    private HashMap<String, String> tokenMap = new HashMap<>();

    public TaggedMsg(Tag tag, String msg)
    {
        this.tag = tag;
        this.meta = msg.split(tag.getName())[0];
        var chanSplit = msg.split(tag.getName())[1].split(":");
        this.channel = chanSplit[0].replace(" ", "");
        if (chanSplit.length > 1)
        {
            this.chat = chanSplit[1];
        } else {
            this.chat = null;
        }
//        this.chat = (String) chanSplit.length?chanSplit[1]:"NAN";

        Arrays.asList(meta.replace("user-type=", "").split(";")).forEach(item ->
        {
            var split = item.split("=");

            if (split.length > 1)
            {
                tokenMap.put(split[0], split[1]);
            } else {
                tokenMap.put(split[0], null);
            }
        });

    }

    public Tag getTag()
    {
        return this.tag;
    }

    public String getChat()
    {
        return this.chat;
    }

    public String getMeta()
    {
        return this.meta;
    }

    public String getChannel()
    {
        return this.channel;
    }

    public String getToken(String token)
    {
        return this.tokenMap.get(token);
    }

    public HashMap<String, String> getTokenMap()
    {
        return this.tokenMap;
    }
}
