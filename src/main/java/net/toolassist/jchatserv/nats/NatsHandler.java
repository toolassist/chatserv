/*
 * All rights reserved.
 *
 * Copyright (c) 2019 Anthony Anderson
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package net.toolassist.jchatserv.nats;

import io.nats.client.Connection;
import io.nats.client.MessageHandler;
import io.nats.client.Nats;
import io.nats.client.Options;
import net.toolassist.jchatserv.config.Config;
import net.toolassist.jchatserv.config.ConfigHelper;
import net.toolassist.jchatserv.nats.listeners.BaseListener;
import net.toolassist.jchatserv.nats.listeners.ConfigListener;
import net.toolassist.jchatserv.nats.publish.BasePublisher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class NatsHandler
{

    private Options options;
    private Config config;
    private Connection natsConnection;
    private HashMap<String, MessageHandler>  msg = new HashMap<>();

    public NatsHandler()
    {
        config = new ConfigHelper().getConfig();
        List<String> servers = new ArrayList<>();
        Arrays.asList(config.natsConf.natsAddresses).forEach(s -> servers.add("nats://" + s));

        options = new Options.Builder().servers(servers.toArray(new String[0])).maxReconnects(-1).build();
        registerListener(new ConfigListener("CONF"));
    }

    /**
     * Registers a nat's message listener for the event bus to listen to
     * @param handler
     */
    private void registerListener(BaseListener handler)
    {
        msg.put(handler.getSubject(), handler);
    }


    public void publishMsg(BasePublisher publisher)
    {
        natsConnection.publish(publisher.getSubject(), publisher.getMsg());
    }

    /**
     * Connect to the nats cluster
     */
    public void connect()
    {
        try
        {
            natsConnection = Nats.connect(this.options);
            initListeners();
        } catch (IOException | InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Disconnect from the nats cluster
     */
    public void disconnect()
    {
        try
        {
            natsConnection.close();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }


    /**
     * initialise all the registered listeners
     */
    private void initListeners()
    {

        msg.forEach((name, msg) ->
        {
           var dist = natsConnection.createDispatcher(msg);
           dist.subscribe(name);
        });
    }


}
