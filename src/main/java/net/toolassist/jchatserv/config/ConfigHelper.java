/*
 * All rights reserved.
 *
 * Copyright (c) 2019 Anthony Anderson
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package net.toolassist.jchatserv.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigHelper
{
    private Gson gson;
    private Config config;
    private Path confFile = Paths.get("chatserv.json");

    public ConfigHelper()
    {
        if (!Files.exists(confFile))
        {
            var gb = new GsonBuilder();

            var conf = new Config();
            var nats = new NatsConf();
            var twitch = new TwitchConf();

            nats.natsAddresses = new String[]{"localhost", "localhost", "localhost"};
            nats.pass = null;
            nats.user = null;

            twitch.defaultChannels = new String[]{"dwangoac", "illyohs"};
            twitch.token = "blarg";
            twitch.user = "blarg";

            conf.node = "jeff";
            conf.natsConf = nats;
            conf.twitchConf = twitch;

            gson = gb.setPrettyPrinting().serializeNulls().create();
            var confStr = gson.toJson(conf);

            try
            {
                Files.write(confFile, confStr.getBytes());
            } catch (IOException e)
            {
                e.printStackTrace();
            }

            System.exit(2);
        }

        try (var reader = Files.newBufferedReader(confFile)) {
            gson = new Gson();
            config = gson.fromJson(reader, Config.class);
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public Config getConfig()
    {
        return this.config;
    }
}
